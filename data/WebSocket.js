var egdoor=0;
var fldoor=0;
var frdoor=0;
var brdoor=0;
var bldoor=0;
var tdoor=0;
var acstatus=1;
var lockstatus=1;

var egdoor2="Close";
var fldoor2="Close";
var frdoor2="Close";
var brdoor2="Close";
var bldoor2="Close";
var tdoor2="Close";
var acstatus2="Close";
var lockstatus2="Close";

var redata="";

var type="";
var carid=0;
var oil= 0;
var egtemp=0;
var intemp=0;
var outtemp= 0;
var speed = 0;
var egrpm= 0;

var socket = new WebSocket('wss://testapi.dennysora.com:8081/socket?ID='+"66693764-26ef-49d9-8faf-6a3de72077ad");

    socket.onopen=function () {
      console.log("socket has been opened");
    };
    socket.onerror = function (error) {
        console.log('WebSocket Error ', error);
    };
    socket.onmessage = function (e) {
        console.log('Server: ', e.data);
        var obj = JSON.parse(e.data);
        type=obj.type;
        speed=obj.km;
        egrpm=obj.krpm;
        egtemp=obj.radiator;
        oil=obj.tank;
        carid=obj.carId;
        egdoor=obj.f;
        fldoor=obj.lf;
        frdoor=obj.rf;
        bldoor=obj.lb;
        brdoor=obj.rb;
        tdoor=obj.b;
        setVR();
    //    socket.send(e.data);
    };
    socket.onclose = function(){
        console.log('WebSocket connection closed');
    };

function startfunction(){
    document.getElementById('egdoor').style.backgroundColor = '#999';
    document.getElementById('fldoor').style.backgroundColor = '#999';
    document.getElementById('frdoor').style.backgroundColor = '#999';
    document.getElementById('bldoor').style.backgroundColor = '#999';
    document.getElementById('brdoor').style.backgroundColor = '#999';
    document.getElementById('tdoor').style.backgroundColor = '#999';
    document.getElementById('acstatus').style.backgroundColor = '#999';
    document.getElementById('lockstatus').style.backgroundColor = '#999';
}

function setVR() {
   document.getElementById('speed').value=speed;
   document.getElementById('carid').value=carid;
   document.getElementById('egrpm').value=egrpm;
   document.getElementById('oil').value=oil;
   document.getElementById('egtemp').value=egtemp;
   document.getElementById('intemp').value=intemp;
   document.getElementById('outtemp').value=outtemp;
   setbutton();
}

function setbutton(){
    if(egdoor==0){
        egdoor=1;
        egdoor2="Open";
        document.getElementById('egdoor').style.backgroundColor = '#00878F';
    }else{
        egdoor=0;
        egdoor2="Close";
        document.getElementById('egdoor').style.backgroundColor = '#999';
    }

    if(fldoor==0){
        fldoor2="Open";
        document.getElementById('fldoor').style.backgroundColor = '#00878F';
    }else{
        fldoor2="Close";
        document.getElementById('fldoor').style.backgroundColor = '#999';
    }

    if(frdoor==0){
        frdoor2="Open";
        document.getElementById('frdoor').style.backgroundColor = '#00878F';
    }else{
        frdoor2="Close";
        document.getElementById('frdoor').style.backgroundColor = '#999';
    }

    if(bldoor==0){
        bldoor2="Open";
        document.getElementById('bldoor').style.backgroundColor = '#00878F';
    }else{
        bldoor2="Close";
        document.getElementById('bldoor').style.backgroundColor = '#999';
    }

    if(brdoor==0){
        brdoor2="Open";
        document.getElementById('brdoor').style.backgroundColor = '#00878F';
    }else{
        brdoor2="Close";
        document.getElementById('brdoor').style.backgroundColor = '#999';
    }

    if(tdoor==0){
        tdoor2="Open";
        document.getElementById('tdoor').style.backgroundColor = '#00878F';
    }else{
        tdoor2="Close";
        document.getElementById('tdoor').style.backgroundColor = '#999';
    }

    if(acstatus==0){
        acstatus2="Open";
        document.getElementById('acstatus').style.backgroundColor = '#00878F';
    }else{
        acstatus2="Close";
        document.getElementById('acstatus').style.backgroundColor = '#999';
    }

    if(lockstatus==0){
        lockstatus2="Open";
        document.getElementById('lockstatus').style.backgroundColor = '#00878F';
    }else{
        lockstatus2="Close";
        document.getElementById('lockstatus').style.backgroundColor = '#999';
    }

}

// document.getElementById('b').className = 'enabled';
        //document.getElementById('r').disabled = false;
        //document.getElementById('rainbow').style.backgroundColor = '#999';
        // document.getElementById('rainbow').style.backgroundColor = '#00878F';
